package com.example.sampleapp;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ijustyce.fastandroiddev.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @Bind(R.id.hello) TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

        textView.setText("hello , welcome ");
    }

    @OnClick(R.id.click)
    public void onClick(View view){

        textView.setText("why click me ?");
        newActivity(QQlike.class);
    }
}
