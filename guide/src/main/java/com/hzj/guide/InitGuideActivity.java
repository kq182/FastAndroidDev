package com.hzj.guide;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BNRoutePlanNode.CoordinateType;
import com.baidu.navisdk.adapter.BaiduNaviManager;
import com.baidu.navisdk.adapter.BaiduNaviManager.NaviInitListener;
import com.baidu.navisdk.adapter.BaiduNaviManager.RoutePlanListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class InitGuideActivity extends Activity {

	private static final String APP_FOLDER_NAME = "BNSDKDemo";
	private String mSDCardPath = null;
	public static final String ROUTE_PLAN_NODE = "routePlanNode";
	private boolean keyRight = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	//	setContentView(R.layout.activity_main);
		initListener();
		if ( initDirs() ) {
			initNavi();
		}
	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	private void initListener() {

	}

	private boolean initDirs() {
		mSDCardPath = getSdcardDir();
		if ( mSDCardPath == null ) {
			return false;
		}
		File f = new File(mSDCardPath, APP_FOLDER_NAME);
		if ( !f.exists() ) {
			try {
				f.mkdir();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	private void initNavi() {
		BaiduNaviManager.getInstance().setNativeLibraryPath(mSDCardPath + "/BaiduNaviSDK_SO");
		BaiduNaviManager.getInstance().init(this, mSDCardPath, APP_FOLDER_NAME,
				new NaviInitListener() {
					@Override
					public void onAuthResult(int status, String msg) {
						if (0 == status) {
							keyRight = true;
						}else{
							keyRight = false;
							Log.e("InitGuideActivity" ,"key is not right ...");
							InitGuideActivity.this.finish();
						}
					}
					public void initSuccess() {
						if (keyRight){
							navigation();
						}
					}

					public void initStart() {
					}

					public void initFailed() {
						InitGuideActivity.this.finish();
					}
				}, null);
	}


	private String getSdcardDir() {
		if (Environment.getExternalStorageState().equalsIgnoreCase(
				Environment.MEDIA_MOUNTED)) {
			return Environment.getExternalStorageDirectory().toString();
		}
		return null;
	}

	private void navigation() {
		BNRoutePlanNode sNode = null;
		BNRoutePlanNode eNode = null;
		sNode = new BNRoutePlanNode(120.1104308776,30.3009545457,
				"御峰大厦", null, CoordinateType.GCJ02);
		eNode = new BNRoutePlanNode(120.1113968776,30.3021915457,
				"萍水街拱苑路口", null, CoordinateType.GCJ02);

//		sNode = new BNRoutePlanNode(116.30142, 40.05087,
//				"百度大厦", null, CoordinateType.GCJ02);
//		eNode = new BNRoutePlanNode(116.39750, 39.90882,
//				"北京天安门", null, CoordinateType.GCJ02);

		if (sNode != null && eNode != null) {
			List<BNRoutePlanNode> list = new ArrayList<>();
			list.add(sNode);
			list.add(eNode);
			BaiduNaviManager.getInstance().
					launchNavigator(this, list, 1, true, new DemoRoutePlanListener(sNode));
		}
	}

	public class DemoRoutePlanListener implements RoutePlanListener {

		private BNRoutePlanNode mBNRoutePlanNode = null;
		public DemoRoutePlanListener(BNRoutePlanNode node){
			mBNRoutePlanNode = node;
		}

		@Override
		public void onJumpToNavigator() {
			Intent intent = new Intent(InitGuideActivity.this, GuideActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable(ROUTE_PLAN_NODE,  mBNRoutePlanNode);
			intent.putExtras(bundle);
			startActivity(intent);
		}
		@Override
		public void onRoutePlanFailed() {
			Log.e("InitGuideActivity", "onRoutePlanFailed()");
		}
	}
}