
package com.ijustyce.fastandroiddev.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.view.KeyEvent;

import com.ijustyce.fastandroiddev.IApplication;
import com.ijustyce.fastandroiddev.unit.Constant;

import java.util.List;

import butterknife.ButterKnife;

/**
 * base Activity for all Activity
 *
 * @author yc
 */
public class BaseActivity extends Activity {

    public IApplication app; //  application object for all activity
    public String TAG;
    public Activity context;

    /**
     * onCreate .
     */
    @Override
    protected void onCreate(Bundle bundle) {

        super.onCreate(bundle);
        context = this;
        app = (IApplication) getApplication();

        TAG = getLocalClassName();
        app.pushActivity(this);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        app.moveActivity(this);

        if (context != null){
            ButterKnife.unbind(context);
            context = null;
        }
    }


    /**
     * is service running ...
     * @param context context
     * @param className packageName + service class name like: com.jilvinfo.crash.CrashReport
     * @return true if running or return false
     */
    public boolean isServiceRun(Context context , String className){
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> list = am.getRunningServices(30);
        for(ActivityManager.RunningServiceInfo info : list){
            if(info.service.getClassName().equals(className)){
                return true;
            }
        }
        return false;
    }

    public String getResString(int id){

        return getResources().getString(id);
    }

    @Override
    public void finish() {
        super.finish();
    }

    /**
     * we start Password class for result , now we deal with the result .
     * If result is cancel , it mean user exit app without passed . so we should
     * finish app and call system.gcc to clean memory .
     */
    public void onActivityResult(int reqCode, int resultCode, Intent data) {

        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {

            case 0:


            default:
                break;
        }
    }

    /**
     * logout .
     */
    public void logOut() {

     //   startService(new Intent(this, ExitService.class));

        Intent intent = new Intent(Constant.RELOGIN);
        sendBroadcast(intent);
    }

    public void newActivity(Class<?> gotoClass){

        startActivity(new Intent(this, gotoClass));
    }

    /**
     * get signInfo by packageName
     *
     * @return sign information
     */
    public String getSignInfo() {

        if (context == null){
            context = this;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(),
                            PackageManager.GET_SIGNATURES);
            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];

            return sign.hashCode() + "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void backPress(){
       // app.finishObject(getLocalClassName());
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            backPress();
        }
        return super.onKeyDown(keyCode, event);
    }
}
