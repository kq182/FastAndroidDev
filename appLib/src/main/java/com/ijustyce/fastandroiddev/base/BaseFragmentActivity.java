package com.ijustyce.fastandroiddev.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;

import com.ijustyce.fastandroiddev.IApplication;

import butterknife.ButterKnife;

/**
 * Created by yc on 2015/8/14.
 */
public class BaseFragmentActivity extends FragmentActivity implements ViewPager.OnPageChangeListener,
        View.OnClickListener {

    public IApplication app; //  application object for all activity
    public String TAG;
    public Activity context;

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onCreate(Bundle bundle){

        super.onCreate(bundle);
        context = this;
        app = (IApplication) getApplication();

        TAG = getLocalClassName();
        app.pushActivity(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        app.moveActivity(this);

        if (context != null){
            ButterKnife.unbind(context);
            context = null;
        }
    }

    public String getResString(int id){

        return getResources().getString(id);
    }

    public void newActivity(Class<?> gotoClass){

        startActivity(new Intent(this, gotoClass));
    }

    public void backPress(){
        // app.finishObject(getLocalClassName());
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            backPress();
        }
        return super.onKeyDown(keyCode, event);
    }
}
