package com.ijustyce.fastandroiddev.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by yc on 2015/8/14.
 */
public class BaseFragment extends Fragment {

    public Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = getActivity().getBaseContext();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (context != null){
            ButterKnife.unbind(context);
            context = null;
        }
    }

    public String getResString(int id){

        if (context == null){
            context = getActivity();
        }
        return context.getResources().getString(id);
    }

    public void newActivity(Class<?> gotoClass){

        startActivity(new Intent(context, gotoClass));
    }
}
