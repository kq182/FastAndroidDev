package com.ijustyce.fastandroiddev.unit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yc on 2015/8/14.
 */
public class RegularUnit {

    public static boolean isMobilePhone(String s){

        if (s == null){
            return false;
        }

        Pattern p = Pattern
                .compile("^((13[0-9])|(15[^4,\\D])|(18[0-9])|(145)|(147)|(17[6-8]))\\d{8}$");
        Matcher m = p.matcher(s);
        return m.matches();
    }
    public static boolean isFixedPhone(String s){

        if (s == null){
            return false;
        }

        Pattern p = Pattern.compile("^([0-9]{3,5}(-)?)?([0-9]{7,8})((-)?[0-9]{1,4})?$");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    public static boolean isEmail(String s){

        if (s == null){
            return false;
        }

        Pattern p = Pattern
                .compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}"
                        + "\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    public static String delStringBlank(String s){

        if (s == null){
            return null;
        }
        return s.replaceAll(" ", "");
    }
}
