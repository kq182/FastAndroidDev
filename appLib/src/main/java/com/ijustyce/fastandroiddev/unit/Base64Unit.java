package com.ijustyce.fastandroiddev.unit;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;

public class Base64Unit {
 
 
    /**
     * 将二进制数据编码为BASE64字符串
     * @param value
     * @return
     */
    public static String encode(String value) {
        try {
            return new String(Base64.encodeBase64(value.getBytes()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
     
    /**
     * 将BASE64字符串恢复为二进制数据
     * @param base64String
     * @return
     */
    public static String decode(String base64String) {
        try {
            return new String(Base64.decodeBase64(base64String.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
 
}