/**
 * date:2014-04-21
 * rewrite ToastUnit
 */
package com.ijustyce.fastandroiddev.unit;

import android.app.ActivityManager;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ijustyce.fastandroiddev.R;

import java.util.List;

public class ToastUnit {

    private static String PKG;
    private static Context context;

    public static void setPkg(String pkg, Context context){

        ToastUnit.PKG = pkg;
        ToastUnit.context = context;
    }

    private static boolean  isTop(){

        ActivityManager activityManager = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> appTask = activityManager.getRunningTasks(1);

        if (appTask != null && appTask.size() > 0){
            if (appTask.get(0).topActivity.toString().contains(PKG)){
                return true;
            }
        }
        return false;
    }

	public static void show(int id, Context context) {

		LayoutInflater mInflater = LayoutInflater.from(context);
		View toastRoot = mInflater.inflate(R.layout.toast, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);
		message.setText(id);

		Toast toastStart = new Toast(context);
		toastStart.setGravity(Gravity.BOTTOM , 0, 90);
		toastStart.setDuration(Toast.LENGTH_LONG);
		toastStart.setView(toastRoot);
		toastStart.show();
	}

	public static void show(Context context, String text) {

		LayoutInflater mInflater = LayoutInflater.from(context);
		View toastRoot = mInflater.inflate(R.layout.toast, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);
		message.setText(text);

		Toast toastStart = new Toast(context);
		toastStart.setGravity(Gravity.BOTTOM , 0, 90);
		toastStart.setDuration(Toast.LENGTH_LONG);
		toastStart.setView(toastRoot);
		toastStart.show();
	}
	
	/**
	 * 
	 * @param id
	 * @param context
	 */
	public static void showTop(int id, Context context) {

        if(!isTop()){
            return;
        }

		LayoutInflater mInflater = LayoutInflater.from(context);
		View toastRoot = mInflater.inflate(R.layout.toast, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);
		message.setText(id);

		Toast toastStart = new Toast(context);
		toastStart.setGravity(Gravity.BOTTOM , 0, 90);
		toastStart.setDuration(Toast.LENGTH_LONG);
		toastStart.setView(toastRoot);
        toastStart.show();
	}

	public static void showTop(Context context, String text) {

        if(!isTop()){
            return;
        }

		LayoutInflater mInflater = LayoutInflater.from(context);
		View toastRoot = mInflater.inflate(R.layout.toast, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);
		message.setText(text);

		Toast toastStart = new Toast(context);
		toastStart.setGravity(Gravity.BOTTOM , 0, 90);
		toastStart.setDuration(Toast.LENGTH_LONG);
		toastStart.setView(toastRoot);
		toastStart.show();
	}
	
	/**
	 * 
	 * @param id
	 * @param yOffset dp , height of ToastUnit
	 * @param context
	 */
	public static void showTop(int id, Context context , int yOffset) {

        if(!isTop()){
            return;
        }

		LayoutInflater mInflater = LayoutInflater.from(context);
		View toastRoot = mInflater.inflate(R.layout.toast_top, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);
		message.setText(id);

		Toast toastStart = new Toast(context);
		toastStart.setGravity(Gravity.TOP , 0, yOffset);
		toastStart.setDuration(Toast.LENGTH_LONG);
		toastStart.setView(toastRoot);
		toastStart.show();
	}

	public static void showTop(Context context, String text , int yOffset) {

        if(!isTop()){
            return;
        }

		LayoutInflater mInflater = LayoutInflater.from(context);
		View toastRoot = mInflater.inflate(R.layout.toast_top, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);
		message.setText(text);

		Toast toastStart = new Toast(context);
		toastStart.setGravity(Gravity.TOP , 0, yOffset);
		toastStart.setDuration(Toast.LENGTH_LONG);
		toastStart.setView(toastRoot);
		toastStart.show();
	}
}
