#FastAndroidDev
这是一个Android极速开发框架，示例Activity采用一体化状态栏，项目包括：greenDao、下拉刷新上拉加载更多、网络请求、图片缓存、Gson等。

##150812：##
1、今天刚建的仓库，代码包含：一体化状态栏、greenDao（包括java部分）、下拉刷新与上拉加载更多、网络请求、图片缓存、Gson、崩溃日志、二维码，图片处理、Application类、BaseActivity、BaseService、文件上传等。